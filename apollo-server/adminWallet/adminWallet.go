package adminWallet

import (
	"fmt"
	"os"
	"strings"

	"github.com/blinklabs-io/bursa"
)

var adminWallet = &bursa.Wallet{}

func Setup() {
	// Setup wallet
	// seedPhrase := os.Getenv("SEED")

	filePath := "/root/Desktop/seedFile.txt"

	fileContent, err := os.ReadFile(filePath)
	if err != nil {
		fmt.Printf("Error reading: %s", err)
		return
	}
	seedPhrase := string(fileContent)

	words := strings.Fields(seedPhrase)

	seedString := strings.Join(words, " ")

	mnemonic := seedString
	rootKey, err := bursa.GetRootKeyFromMnemonic(mnemonic)
	if err != nil {
		panic(err)
	}
	accountKey := bursa.GetAccountKey(rootKey, 0)
	paymentKey := bursa.GetPaymentKey(accountKey, 0)
	stakeKey := bursa.GetStakeKey(accountKey, 0)
	addr := bursa.GetAddress(accountKey, "preprod", 0)
	wallet := &bursa.Wallet{
		Mnemonic:       mnemonic,
		PaymentAddress: addr.String(),
		StakeAddress:   addr.ToReward().String(),
		PaymentVKey:    bursa.GetPaymentVKey(paymentKey),
		PaymentSKey:    bursa.GetPaymentSKey(paymentKey),
		StakeVKey:      bursa.GetStakeVKey(stakeKey),
		StakeSKey:      bursa.GetStakeVKey(stakeKey),
	}
	adminWallet = wallet
}

func GetWallet() *bursa.Wallet {
	return adminWallet
}
