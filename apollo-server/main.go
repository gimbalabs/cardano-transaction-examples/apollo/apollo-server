package main

import (
	"apollo-server/adminWallet"
	"encoding/hex"
	"fmt"

	"github.com/Salvionied/apollo"
	"github.com/Salvionied/apollo/serialization/Address"
	"github.com/Salvionied/apollo/serialization/Key"
	"github.com/Salvionied/apollo/txBuilding/Backend/BlockFrostChainContext"
	"github.com/Salvionied/apollo/txBuilding/Utils"
)

func main() {
	sendLovelaceToMyLover()
}

const senderBech32Address string = "addr_test1qqes85ckxz7ldrmcgeyynrz3whkna8pdyu9px7hs7gj6aykyfdsx760rsuyckgrwyrrcena3vl40g2pxe6qkjl5ylduqsglcc5"

const receiverBech32Address_1 string = "addr_test1qz9al9txw955x4hprfnflm0nqavlxu6k3hf97j63p0x4jqv4cxj9qnxul9xv3rgytj6ln8zcruak8rj6uryfdpy2nnzskdpj7q"

const receiverBech32Address_2 string = "addr_test1qrzq3myzsg94w6m6zkucc3q92lv4funx09e7vvcpt6h7q590gvjwt0fmxml4stsmrpmkey9h39ma6l0axrmsq4deym8qhsvw8q"

func sendLovelaceToMyLover() {
	bfc := BlockFrostChainContext.NewBlockfrostChainContext("https://cardano-preprod.blockfrost.io/api", int(0), "preprod9zzl4g8Xa3faU50a1OVDZdPeQ92ZsdcT")

	// cc := apollo.NewEmptyBackend()

	apolloBE := apollo.New(&bfc)

	apolloBE = apolloBE.SetWalletFromBech32(senderBech32Address).SetWalletAsChangeAddress()

	senderAddress := *apolloBE.GetWallet().GetAddress()

	senderUTxO := bfc.Utxos(senderAddress)

	receiverAddress_2, err := Address.DecodeAddress(receiverBech32Address_2)
	if err != nil {
		fmt.Println(err)
	}

	lastSlot := bfc.LastBlockSlot()

	adminWallet.Setup()
	admin_wallet := adminWallet.GetWallet()
	fmt.Println(admin_wallet.Mnemonic)
	
	vKeyBytes, err := hex.DecodeString(admin_wallet.PaymentVKey.CborHex)
	if err != nil {
		fmt.Println(err)
	}
	sKeyBytes, err := hex.DecodeString(admin_wallet.PaymentSKey.CborHex)
	if err != nil {
		fmt.Println(err)
	}
	// Strip off leading 2 bytes as shortcut for CBOR decoding to unwrap bytes
	vKeyBytes = vKeyBytes[2:]
	sKeyBytes = sKeyBytes[2:]
	// Strip out public key portion of extended private key
	sKeyBytes = append(sKeyBytes[:64], sKeyBytes[96:]...)
	vkey := Key.VerificationKey{Payload: vKeyBytes}
	skey := Key.SigningKey{Payload: sKeyBytes}

	apolloBE, err = apolloBE.
		AddLoadedUTxOs(senderUTxO...).
		PayToAddressBech32(receiverBech32Address_1, int(150000000)).
		PayToAddress(receiverAddress_2, int(170000000)).
		AddRequiredSignerFromAddress(senderAddress, true, false).
		AddRequiredSignerFromBech32(admin_wallet.PaymentAddress, true, false).
		SetTtl(int64(lastSlot) + 5000).
		Complete()

	if err != nil {
		fmt.Println(err)
	}

	apolloBE, err = apolloBE.SignWithSkey(vkey, skey)
	if err != nil {
		fmt.Println(err)
	}

	cborString := Utils.ToCbor(apolloBE.GetTx())
	fee := apolloBE.GetTx().TransactionBody.Fee

	fmt.Println(cborString)
	fmt.Println("-------------------------------------------------------------------------")
	fmt.Println(fee)

}
